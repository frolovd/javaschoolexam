package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        StringBuilder tempString = new StringBuilder();
        StringBuilder tempNum = new StringBuilder();
        Stack<Character> opStack = new Stack<>();
        Stack<Double> numStack = new Stack<>();

        if (statement == null || statement.isEmpty()) {
            return null;
        }

        if (!isValid(statement)) {
            return null;
        }

        for (int i = 0; i < statement.length(); i++) {
            if (precedenceResolver(statement.charAt(i)) == 0) {
                tempString.append(statement.charAt(i));
            } else if (precedenceResolver(statement.charAt(i)) == 2) {
                opStack.push(statement.charAt(i));
            } else if (precedenceResolver(statement.charAt(i)) >= 3) {
                tempString.append(" ");
                while (!opStack.empty()) {
                    if (precedenceResolver(opStack.peek()) >= precedenceResolver(statement.charAt(i))) {
                        tempString.append(opStack.pop());
                    } else break;
                }
                opStack.push(statement.charAt(i));
            } else if (precedenceResolver(statement.charAt(i)) == 1) {
                tempString.append(" ");
                while (precedenceResolver(opStack.peek()) != 2) {
                    tempString.append(opStack.pop());
                }
                opStack.pop();
            } else {
                return null;
            }
        }

        while (!opStack.empty()) {
            tempString.append(opStack.pop());
        }

        for (int i = 0; i < tempString.length(); i++) {
            if (precedenceResolver(tempString.charAt(i)) == 0) {
                while (tempString.charAt(i) != ' ' && precedenceResolver(tempString.charAt(i)) == 0) {
                    tempNum.append(tempString.charAt(i++));
                }
                if (i == tempString.length()) {
                    break;
                }
                numStack.push(Double.valueOf(tempNum.toString()));
                tempNum.replace(0, tempNum.length(), "");
            }

            if (precedenceResolver(tempString.charAt(i)) >= 3) {
                double b = numStack.pop();
                double a = numStack.pop();
                switch (tempString.charAt(i)) {
                    case '+':
                        numStack.push(a + b);
                        break;
                    case '-':
                        numStack.push(a - b);
                        break;
                    case '*':
                        numStack.push(a * b);
                        break;
                    case '/': {
                        numStack.push(a / b);
                        if (b == 0) {
                            return null;
                        }
                        break;
                    }
                }
            }
        }

        double result = numStack.pop();

        if (isInteger(result)) {
            return Integer.toString((int) result);
        }

        return Double.toString(result);
    }

    private static boolean isValid(String expression) {

        int bracketsCount = 0;

        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '.') {
                if (expression.charAt(i + 1) == '.') {
                    return false;
                }
            }

            if (precedenceResolver(expression.charAt(i)) >= 3) {
                if (precedenceResolver(expression.charAt(i + 1)) >= 3) {
                    return false;
                }
            }
            if (expression.charAt(i) == '(' || expression.charAt(i) == ')') {
                bracketsCount++;
            }
        }

        if (bracketsCount % 2 == 0 || bracketsCount == 0) {
            return true;
        }
        return false;
    }

    private static int precedenceResolver(char character) {

        if (character == '*' || character == '/') {
            return 4;
        } else if (character == '+' || character == '-') {
            return 3;
        } else if (character == '(') {
            return 2;
        } else if (character == ')') {
            return 1;
        } else if (Character.isDigit(character) || character == '.') {
            return 0;
        }
        return -1;
    }

    private static boolean isInteger(double number) {

        if (number % 1 == 0) {
            return true;
        }
        return false;
    }
}
