package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int rows = 0;
        int elementsInRow = 1;
        int size = inputNumbers.size();

        while (size > 0){
            size -= elementsInRow;
            rows++;
            elementsInRow++;
        }

        if (inputNumbers.contains(null) || size < 0) throw new CannotBuildPyramidException();

        int columns = 2 * elementsInRow - 3;

        Collections.sort(inputNumbers);

        int[][] result = new int[rows][columns];

        for(int i = 0; i < rows; i++)
            for(int j = 0; j < columns; j++)
                result[i][j] = 0;

        elementsInRow = 1;
        int startIndex = columns / 2;
        int temp = 0;

        for (int i = 0; i < rows; i++){
            int currentIndex = startIndex;
            for (int j = 0; j < elementsInRow; j++){
                result[i][currentIndex] = inputNumbers.get(temp);
                temp++;
                currentIndex += 2;
            }
            startIndex--;
            elementsInRow++;
        }

        return result;
    }
}
